# Jenkins GitLab Integration
**This pipeline automation setup will,**
* Poll on code commit or merge in GitLab (all branches) to trigger Jenkins multibranch Pipelines. 
* Publish Jenkins Pipeline Stages status back in GitLab. (CI/CD -> Pipeline, Charts)
* Delete Jenkins Pipeline on Branch deletion to avoid manual cleanup.
* Discard Builds keeping only last 10 in history to save on space.

**Steps:**
1. Install GitLab Plugin in Jenkins.
1. Create API Token: GitLab -> Settings (Profile) -> Access Token.
2. Add SSH: GitLab -> Setting (Profile) -> SSH Keys. Public Key (.pub) at GitLab and Private at Jenkins Global Creds.
2. Add API Token and SSH key at Jenkins Global Credentials.
1. Create multibranch Job in Jenkins: Configure -> Branches Source (git). Provide GitLab Branch for polling. 
1. Webhooks Configuration: GitLab -> Settings (Project) -> Integration (Jenkins Job URL with format https://fqdn/project/job. Note, project is not part of actual Jenkins URL.
1. Select Triggers as required for Webhooks like Push Event and Merge Request Event in this scenarios.
